package com.itau.geometria.formas;

public class Circulo extends Forma{
	private double raio;

	@Override
	public double calcularArea() {
		return Math.pow(raio, 2) * Math.PI;
	}
	
	public Circulo (double raio) {
		this.raio = raio;
	}
}
