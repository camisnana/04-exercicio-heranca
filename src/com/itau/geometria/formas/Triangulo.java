package com.itau.geometria.formas;

public class Triangulo extends Forma {
	private  double ladoA;
	private  double ladoB;
	private  double ladoC;
	
	@Override
	public double calcularArea() {
		double s = (ladoA + ladoB + ladoC) / 2;       
		Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
		return s;
	}
	public Triangulo (double ladoA, double ladoB, double ladoC) throws Exception {
		this.ladoA = ladoA;
		this.ladoB = ladoB;
		this.ladoC = ladoC;
		
		if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA == true) {
			throw new Exception ("Não é um triângulo válido");
			 
		}
	}
}
