package com.itau.geometria.formas;

public class Retangulo extends Forma{
	private double largura;
	private double altura;

	@Override
	public double calcularArea() {
		return altura * largura;
	}

	public Retangulo (double largura, double altura) {
		this.largura = largura;
		this.altura = altura;
	}
}
