package com.itau.geometria;

import com.itau.geometria.formas.Circulo;
import com.itau.geometria.formas.Forma;
import com.itau.geometria.formas.Retangulo;
import com.itau.geometria.formas.Triangulo;

public class App {
	public static void main(String[] args) throws Exception {
		Forma circulo = new Circulo(3);
		Forma retangulo = new Retangulo(3, 4);
	
		try {
			Forma triangulo = new Triangulo(1, 2, 1);
			System.out.println("A área do Triangulo é : " + triangulo);
		} 
		catch(Exception e) {
			System.out.println("NÃO É TRIÂNGULO");
		}
		
		System.out.println("A área do Circulo é   : " + circulo);
		System.out.println("A área do Retângulo é : " + retangulo);		
	}

}
